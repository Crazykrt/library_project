<?php

namespace app\controllers;

use app\models\OrdersModel;
use app\models\ReservationForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        Yii::$app->name = 'Library';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $ordersModel = new OrdersModel();
        $users = $ordersModel->getUsers();
        $books = $ordersModel->getBooks();
        $orders = $ordersModel->getOrders();

        $form = new ReservationForm();
        $form->start = $today = date('Y-m-d');
        $form->end = date('Y-m-d', strtotime('+7 days'));
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $failed = false;
            if (!($user = $ordersModel->getUser($form->user))) {
                $form->addError('user', 'User not found');
                $failed = true;
            }
            if (!($book = $ordersModel->getBook($form->book))) {
                $form->addError('book', 'Book not found');
                $failed = true;
            }
            if (strtotime($form->start) > strtotime($form->end)) {
                $form->addError('end', 'Return date must be more than borrow date');
                $failed = true;
            }

            if (!$failed && $response = $ordersModel->saveOrder($form)) {
                Yii::$app->session->setFlash('reservationForm');
                return $this->refresh();
            } else {
                $form->addError('end', 'Interval is NOT available');
            }
        }
        return $this->render('index', [
            'users' =>  $users,
            'books' => $books,
            'orders' => $orders,
            'reservationForm' => $form,
            'statistics' => $ordersModel->statistics()
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
