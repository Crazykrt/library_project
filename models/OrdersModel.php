<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class OrdersModel extends Model
{
    private $db;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->db = Yii::$app->db;
    }

    public function getUsers($limit = 100)
    {
        try {
            return Yii::$app->db->createCommand('SELECT * FROM users ORDER BY id ASC LIMIT :limit')->bindValue(':limit', $limit)->queryAll();
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
            return [];
        }
    }

    public function getBooks($limit = 100)
    {
        try {
            return Yii::$app->db->createCommand('SELECT * FROM books ORDER BY id ASC LIMIT :limit')->bindValue(':limit', $limit)->queryAll();
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
            return [];
        }
    }

    public function getOrders($limit = 100)
    {
        try {
            return Yii::$app->db->createCommand('SELECT orders.*, users.name as user_name, books.name as book_name  FROM orders join users on orders.user_id = users.id join books on orders.book_id = books.id ORDER BY start DESC LIMIT :limit')->bindValue(':limit', $limit)->queryAll();
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }

    public function saveOrder($form)
    {
        $exist = Yii::$app->db->createCommand('SELECT * FROM orders WHERE book_id = :book and ((:start_date BETWEEN orders.start and orders.end) or (:end_date BETWEEN orders.start and orders.end))')
            ->bindValue(':book', $form->book)
            ->bindValue(':start_date', $form->start)
            ->bindValue(':end_date', $form->end)
            ->queryOne();
        if ($exist) {
            return false;
        }
        return Yii::$app->db->createCommand()->insert('orders', [
            'book_id' => $form->book,
            'user_id' => $form->user,
            'start' => $form->start,
            'end' => $form->end
        ])->execute();
    }

    public function getUser($id)
    {
        return  Yii::$app->db->createCommand('SELECT * FROM users WHERE id=:id LIMIT 1')->bindValue(':id', $id)->queryOne();
    }

    public function getBook($id)
    {
        return  Yii::$app->db->createCommand('SELECT * FROM books WHERE id=:id LIMIT 1')->bindValue(':id', $id)->queryOne();
    }

    public function statistics()
    {
        $data = [];
        $data[] = Yii::$app->db->createCommand('SELECT orders.user_id, count(*) as books, users.name as user_name FROM orders join users on orders.user_id = users.id group by orders.user_id order by books DESC LIMIT 1')->queryOne();
        $data[] = Yii::$app->db->createCommand('SELECT orders.book_id, count(*) as times, books.name as book_name FROM orders join books on orders.book_id = books.id group by orders.book_id order by times DESC LIMIT 1')->queryOne();
        $data[] = Yii::$app->db->createCommand('select book_id, book_name, count(*) as count from (SELECT orders.*, books.name as book_name FROM orders join books on books.id = orders.book_id group by orders.book_id, orders.user_id) f group by book_id order by count DESC limit 1')->queryOne();
        return $data;
    }
}
