<?php

namespace app\models;

use Yii;
use yii\base\Model;


class ReservationForm extends Model
{
    public $user;
    public $book;
    public $start;
    public $end;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['user', 'book', 'start', 'end'], 'required']

        ];
    }
}
