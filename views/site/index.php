<?php

/* @var $this yii\web\View */

use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Library admin';
$session = Yii::$app->session;
?>
<div class="site-index">

    <!--<div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>-->

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2>Users</h2>

                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <th scope="row"><?= $user['id'] ?></th>
                            <td><?= $user['name'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-3">
                <h2>Books</h2>

                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($books as $book) : ?>
                        <tr>
                            <th scope="row"><?= $book['id'] ?></th>
                            <td><?= $book['name'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-3">
                <?php
                    $form = ActiveForm::begin();
                    $select = [];
                    foreach ($users as $key => $user) {
                        $select[$user['id']] = $user['name'];
                    }
                    echo $form->field($reservationForm, 'user')->dropdownList($select,
                        ['prompt'=>'Select user']);

                    $select = [];
                    foreach ($books as $key => $book) {
                        $select[$book['id']] = $book['name'];
                    }
                    echo $form->field($reservationForm, 'book')->dropdownList($select,
                        ['prompt'=>'Select book']);
                ?>

                <?= $form->field($reservationForm, 'start')->input('date') ?>

                <?= $form->field($reservationForm, 'end')->input('date') ?>


                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

                <?php
                    if (Yii::$app->session->hasFlash('reservationForm')) {
                        echo Alert::widget([
                            'options' => ['class' => 'alert-info'],
                            'body' => "You have successfully added a reservation",
                        ]);
                    }
                ?>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <h2>Statistics</h2>
                <table class="table table-dark">
                    <tbody>
                    <?php if ($statistics[0] && $statistics[1] && $statistics[2]) { ?>
                        <tr>
                            <th scope="row">User borrows the most</th>
                            <td><?= $statistics[0]['user_name'] ?> (<?= $statistics[0]['books'] ?> times)</td>
                        </tr>
                        <tr>
                            <th scope="row">Most borrowed book </th>
                            <td><?= $statistics[1]['book_name'] ?> (<?= $statistics[1]['times'] ?> times)</td>
                        </tr>
                        <tr>
                            <th scope="row">Most borrowed book by different people </th>
                            <td><?= $statistics[2]['book_name'] ?> (<?= $statistics[2]['count'] ?> users)</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-8">
                <h2>Orders</h2>
                <p><i style="background:#eaeaea">grey - ended</i>, <i style="background:#caf7ca">green - ongoing</i>, <i style="background:#ffb">yellow - incoming</i></p>
                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Book</th>
                        <th scope="col">Booking time</th>
                        <th scope="col">Return time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($orders as $order) : ?>
                        <tr style="background: <?php echo ordersRowFilter($order['start'], $order['end']) ?>">
                            <th scope="row"><?= $order['id'] ?></th>
                            <td><a href="#user"><?= $order['user_name'] ?></a></td>
                            <td><a href="#book"><?= $order['book_name'] ?></a></td>
                            <td><?= $order['start'] ?></td>
                            <td><?= $order['end'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<?php

function ordersRowFilter ($start, $end)
{
    $startDate = strtotime($start);
    $endDate = strtotime($end);
    $now = time();
    if ($startDate > $now) {
        return "#ffb";
    } elseif ($startDate < $now && $endDate > $now) {
        return "#caf7ca";
    } else {
        return "#eaeaea";
    }
}